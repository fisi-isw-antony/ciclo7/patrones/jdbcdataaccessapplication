<%-- 
    Document   : index
    Created on : 20/08/2019, 05:23:15 PM
    Author     : LaboratorioFISI
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="jdbcdataaccessapplication.JDBCDataAccessClass" %>
<%@page import="java.util.ArrayList"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        
        <%
            
            JDBCDataAccessClass jdbc = new JDBCDataAccessClass();
            jdbc.initialize();
            ArrayList<String> lista = jdbc.listStaffJSP();
            String list = null;
            out.println(lista);
            
            for (String nn: lista) {
                list = nn + "\n";
            }
            
            out.print(list);
        %>
    </body>
</html>
